package com.devcamp.stringlengthapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringLengthApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringLengthApiApplication.class, args);
	}

}
