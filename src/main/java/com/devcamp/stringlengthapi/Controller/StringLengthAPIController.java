package com.devcamp.stringlengthapi.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin

public class StringLengthAPIController {
    @GetMapping("/length")
    public int StringLengthAPI (@RequestParam(required= true, name= "string") String request ){
        return request.length();
    }

    // @GetMapping("/length")
    // public int getStringLength(@RequestParam String inputStr) {
    //     return inputStr.length();
    // }

}
//(required= true, name= "string")